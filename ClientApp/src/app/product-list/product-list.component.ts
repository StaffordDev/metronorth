// Component for listing products. Link to Add to product list.

import { Component, Inject, OnInit } from '@angular/core';

import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Product } from '../../entities/product';

import { ProductService } from '../../services/product.service';

import { ProductComponent } from '../product-dialog/product-dialog.component';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html'
})
export class ProductListComponent implements OnInit {

  private products: Product[];

  constructor(
    private productService: ProductService,
    public dialog: MatDialog
  ) { }

  addProduct(): void {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '40%';
    dialogConfig.data = new Product();

    const dialogRef = this.dialog.open(ProductComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.products = this.productService.findAll();
    });
  }

  clearList(): void {    
    this.products = this.productService.removeAll();
  }

  removeProduct(index): void {
    this.products = this.productService.removeProductAtIndex(index);
  }

  ngOnInit() {
    this.products = this.productService.findAll();
  }
}
