// Component to display calculated receipt information.

import { Component, Inject, OnInit } from '@angular/core';

import { Product } from '../../entities/product';

import { ProductService } from '../../services/product.service';

@Component({
  selector: 'receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.css']
})
export class ReceiptComponent implements OnInit {

  private products: Product[];
  totalSalesTax: number = 0;
  grandTotal: number = 0;

  constructor(
    private productService: ProductService
  ) { }

  ngOnInit() {
    // Get all the products and calculate totals.
    this.products = this.productService.findAll();

    this.products.forEach((product) => { this.totalSalesTax += (+product.salesTax + +product.importTax) * +product.quantity })
    this.products.forEach((product) => { this.grandTotal += +product.total })

    // Could redirect on page load if no items
  }
}
