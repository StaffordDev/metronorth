// Component to add new product to basket.

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Product } from '../../entities/product';
import { ProductService } from '../../services/product.service';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.css']
})
export class ProductComponent implements OnInit {

  form: FormGroup;

  constructor(
    private productService: ProductService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<ProductComponent>,
    @Inject(MAT_DIALOG_DATA) { name, cost, salesExcempt, isImport, quantity }: Product) {

    this.form = this.fb.group({
      name: ['', Validators.required],
      cost: [0, Validators.required],
      salesExcempt: [false],
      isImport: [false],
      quantity: [1, [Validators.required, Validators.min(1)]]
    });

  }

  cancel(): void {
    this.dialogRef.close();
  }

  create(): void {
    if (this.form.valid) {
      console.log(this.form.value);

      let newProduct = new Product;
      newProduct.name = this.form.value.name;
      newProduct.isImport = this.form.value.isImport ? true : false;
      newProduct.salesExcempt = this.form.value.salesExcempt ? true : false;
      // Some rounding required here.
      newProduct.cost = +this.form.value.cost.toFixed(2);
      newProduct.quantity = Math.round(+this.form.value.quantity);

      this.productService.addProduct(newProduct);
      this.dialogRef.close(this.form.value);
    }
  }

  ngOnInit() {
    
  }

}
