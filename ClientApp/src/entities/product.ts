export class Product {

  name: string;
  salesExcempt: boolean;
  isImport: boolean;
  cost: number;
  salesTax: number;
  importTax: number;
  quantity: number = 1;

  get total() {
    return (+this.cost + +this.salesTax + +this.importTax) * +this.quantity;
  }

}
