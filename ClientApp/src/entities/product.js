"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Product = /** @class */ (function () {
    function Product() {
        this.quantity = 1;
    }
    Object.defineProperty(Product.prototype, "total", {
        get: function () {
            return (+this.cost + +this.salesTax + +this.importTax) * +this.quantity;
        },
        enumerable: true,
        configurable: true
    });
    return Product;
}());
exports.Product = Product;
//# sourceMappingURL=product.js.map