// Service that manages the shopping basket products.

import { Injectable } from '@angular/core';

import { Product } from '../entities/product';

@Injectable()
export class ProductService {

  private products: Product[];

  constructor() {
    this.products = [];

    //{ name: "Demo", isMedical: false, isFood: false, isBook: false, cost: 3.50,}
  }

  findAll(): Product[] {
    return this.products;
  }

  removeAll(): Product[] {
    this.products = [];
    return this.findAll();
  }

  addProduct(product): void {

    product.salesTax = !product.salesExcempt ? Math.ceil((product.cost * .1) * 20) / 20 : 0;
    product.importTax = product.isImport ? Math.ceil((product.cost * .05) * 20) / 20 : 0;

    console.log(product.cost);

    this.products.push(product);
  }

  removeProductAtIndex(index): Product[] {
    if (this.products.length > index) {
      this.products.splice(index, 1);
      return this.findAll();
    }
  } 
  
}
